#!/bin/Rscript

library(ggplot2)
library(stringr)
library(rlang)
suppressMessages(library(dplyr))
args <- commandArgs(TRUE)
res <- read.csv(args[1])
no_ext <- tools::file_path_sans_ext(args[1])
cache_sizes <- c()
matches <- str_match_all(no_ext, "L([0-9])-([0-9]+)")[[1]][,2:3]
cache_sizes[as.numeric(matches[,1])] <- as.numeric(matches[,2])
access_count <- as.integer(str_match(no_ext, "a-([0-9]+)")[2])
repeat_count <- as.integer(str_match(no_ext, "r-([0-9]+)")[2])
cache_line_size <- as.integer(str_match(no_ext, "l-([0-9]+)")[2])
dgemm_size <- as.integer(str_match(no_ext, "ds-([0-9]+)")[2])
p1_cpu_mask <- str_match(no_ext, "p1-(0x([0-9]+))")[2]
p2_cpu_mask <- str_match(no_ext, "p2-(0x([0-9]+))")[2]

subt <- sprintf("Average access count: %s; repeat count: %s; dcache line size: %s\nP1 cpu mask: %s; P2 cpu mask: %s\nDgemm size: %s",
				format(access_count, big.mark=","), format(repeat_count, big.mark=","),
				format(cache_line_size, big.mark=","), p1_cpu_mask, p2_cpu_mask,
				format(dgemm_size, big.mark=","))

# L1size <- cache_sizes[1]
L2size <- cache_sizes[2]
L3size <- cache_sizes[3]

# breaks <- unique(sort(c(#2^(0:floor(log2(L1size))),
# 						#L1size,
# 						#2^((floor(log2(L1size)) + 1):floor(log2(L2size))),
# 						L2size,
# 						2^((floor(log2(L2size)) + 1):floor(log2(L3size))),
# 						L3size,
# 						NA)))

labels_format <- function(x) {
	m <- match(x, cache_sizes)
	m[!is.na(m)] <- paste0("L", m[!is.na(m)], "=", x[!is.na(m)])
	m[is.na(m)] <- x[is.na(m)]
	return(m)
}

colnames(res) <- c("type", "size", "time", "miss")

res_chg <- res %>%
		group_by(type, size) %>%
		summarise(mean_miss = mean(miss), sd_miss = sd(miss)) %>%
		mutate(minv_miss = mean_miss - sd_miss, maxv_miss = mean_miss + sd_miss) %>%
		select(-sd_miss)

breaks <- res_chg[res_chg$type == 1,]$size
breaks <- unique(c(breaks[1], breaks[seq(1, length(breaks), 3)], breaks[length(breaks)]))
labels <- labels_format(breaks)

print(res_chg[res_chg$type==1,], n=Inf)
print(res_chg[which.max(res_chg$mean_miss),])

filename <- sprintf("%s_misses.jpg", no_ext)
print(sprintf("Saving to: %s", filename))
ggsave(filename, height=7, width=14, 
	plot=ggplot(res_chg[res_chg$type==1,], aes(size, mean_miss)) +
	# geom_col(width=0.05) +
	geom_area() +
	geom_vline(xintercept=cache_sizes, alpha=0.2) +
	geom_hline(yintercept=as.numeric(res_chg[res_chg$type==0, "mean_miss"]), alpha=0.7, color="red2") +
	geom_hline(yintercept=as.numeric(res_chg[res_chg$type==0, c("minv_miss", "maxv_miss")]), alpha=0.3, color="red2") +
	geom_errorbar(aes(ymin = minv_miss, ymax = maxv_miss), position="dodge") +
	scale_y_continuous(name = "Cache misses (%)") +
	scale_x_continuous(name = "Parasite array size (B)",
					   breaks = breaks, labels = labels) +
	labs(title = "Cache misses as a function of the array size.",
		 subtitle = subt) +
	theme(axis.text.x = element_text(angle = 45, hjust = 1)))